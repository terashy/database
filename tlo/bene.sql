-- Schule
-- Richard
SHOW DATABASES;
CREATE DATABASE IF NOT EXISTS Schule;
USE Schule;
SHOW TABLES
FROM Schule;
CREATE TABLE klasse (
    klasseId int PRIMARY KEY AUTO_INCREMENT,
    Bezeichnung VARCHAR(22)
);
INSERT INTO klasse (Bezeichnung) value ("1b", "IT");
SELECT *
FROM klasse;
CREATE TABLE IF NOT EXISTS Wochentag (
    WochentagID int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Bezeichnung VARCHAR(22)
);
CREATE TABLE IF NOT EXISTS Schueler (
    SchuelerId int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Vname VARCHAR(22),
    Nachname VARCHAR(22),
    Geburtsdatum DATE,
    klasseId int,
    FOREIGN KEY (klasseId) REFERENCES klasse (klasseId)
);
CREATE TABLE IF NOT EXISTS Dozenten (
    DozentenId int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Vname VARCHAR(22)
);
CREATE TABLE IF NOT EXISTS Modul (
    ModulId int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Bezeichnung VARCHAR(22)
);
CREATE TABLE IF NOT EXISTS Stundenplan (
    UhrzeitStart datetime,
    UhrzeitEnde datetime,
    DozentenId int,
    klasseId int,
    ModulId int,
    RaumId int,
    WochentagId int,
    SchuelerId int,
    CONSTRAINT Dozent FOREIGN KEY (DozentenId) REFERENCES Dozenten (DozentenId),
    CONSTRAINT klasse FOREIGN KEY (klasseId) REFERENCES klasse (klasseId),
    CONSTRAINT Modul FOREIGN KEY (ModulId) REFERENCES Modul (ModulID),
    CONSTRAINT Raum FOREIGN KEY (RaumId) REFERENCES Raum (RaumId),
    CONSTRAINT Wochentag FOREIGN KEY (WochentagId) REFERENCES Wochentag (WochentagId),
    CONSTRAINT Schueler FOREIGN KEY (SchuelerId) REFERENCES Schueler (SchuelerId)
);
CREATE TABLE IF NOT EXISTS Raum (
    RaumId int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Bezeichnung VARCHAR(22),
    Stockwerk VARCHAR(22),
    EDV VARCHAR(22)
);
