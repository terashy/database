-- Schul Bücher
SHOW DATABASES;

CREATE DATABASE IF NOT EXISTS Schul_bibliothek;

USE Schul_bibliothek;

# Erstellen von 2 neuen Tabellen für die Schul-Bibliothek
CREATE TABLE buch (
    ISBN char(10) PRIMARY KEY,
    Name char(33),
    Author VARCHAR(22),
    Verlag VARCHAR(22),
    Jahr date,
    PRIMARY KEY (ISBN)
);

CREATE TABLE schueler(
    SchuelerId int PRIMARY KEY NOT NULL AUTO_INCREMENT,
    Anrede enum('Herr', 'Frau'),
    Vorname varchar(20) NOT NULL,
    Nachname varchar(20) NOT NULL,
    Strasse varchar(25),
    PLZ integer,
    Ort varchar(20),
    Telefon varchar(13),
    Geburtsdatum DATE,
    PRIMARY KEY (SchuelerId)
);

CREATE TABLE buch_schueler (
    ISBN char(10),
    SchuelerId int,
    CONSTRAINT PK_BUCH_SCHUELER PRIMARY KEY (ISBN, SchuelerId),
    CONSTRAINT FK_BUCH_SCHUELER_BUCH FOREIGN KEY (ISBN) REFERENCES buch (isbn),
    CONSTRAINT FK_BUCH_SCHUELER_SCHUELER FOREIGN KEY (SchuelerId) REFERENCES schueler (SchuelerId)
);

CREATE TABLE Bestellung (
    BID int NOT NULL AUTO_INCREMENT,
    Zeit von DATE,
    Zeit bis date,
    Anzahl int,
    CONSTRAINT buch FOREIGN KEY (ISBN) REFERENCES buch(ISBN),
    CONSTRAINT buch_schueler FOREIGN KEY (SchuelerId) REFERENCES (ISBN) EIGN KEY buch (ISBN)
);

# Mockdaten einfügen
INSERT INTO
    buch (ISBN, Name)
VALUES
    ('320764577', 'Things Fall Apart'),
    ('654404044', 'Fairy tales'),
    ('139340034', 'The Divine Comedy'),
    ('159489705', 'The Epic Of Gilgamesh'),
    ('433408815', 'The Book Of Job'),
    ('808442899', 'One Thousand and One Nights'),
    ('763049834', 'Pride and Prejudice'),
    (
        '763022001',
        'Molloy, Malone Dies, The Unnamable, the trilogy'
    );

INSERT INTO
    buch(ISBN, Name) VALUE ('A123456789', 'Star Wars 1'),
    ('B123456789', 'Star Wars 2'),
    ('c123456789', 'Star Wars 3'),
    ('D123456789', 'Star Wars 4'),
    ('E123456789', 'Star Wars 5');

INSERT INTO
    buch(ISBN, Name) VALUE ('F123456789', 'Star Wars 12');

# Alle Bücher abfragen, die mit 'Star Wars' beginnen
SELECT
    *
FROM
    buch
WHERE
    Name LIKE 'Star Wars%';

# Alle Bücher abfragen, die mit 'Star Wars ' beginnen und ein beliebiges Zeichen am Schluss haben
SELECT
    *
FROM
    buch
WHERE
    Name LIKE 'Star Wars _';

# Alle Bücher abfragen, die mit 'Star Wars ' beginnen und ein 2 beliebige Zeichen am Schluss haben
# Es wird somit nur ein Buch angezeigt (Star Wars 12)
SELECT
    *
FROM
    buch
WHERE
    Name LIKE 'Star Wars __';

# Alle Bücher abfragen, die irgendwo im Titel ein 'and' haben
SELECT
    *
FROM
    buch
WHERE
    Name LIKE '%and%';

#VERMERK: LIKE wird bei Abfragen benutzt, um nach einem gewissen Text zu filtern. Niemals ein '='
# Abfragen von mehreren Werten, die die gleiche ISBN haben
SELECT
    *
FROM
    buch
WHERE
    ISBN IN (
        654404044,
        763022001,
        808442899,
        763049834
    );

SELECT
    *
FROM
    buch
WHERE
    ISBN = 654404044
    OR ISBN = 763022001
    OR ISBN = 808442899
    OR ISBN = 763049834;

SELECT
    *
FROM
    buch
WHERE
    Name IN (
        'Star Wars 1',
        'Star Wars 2',
        'Star Wars 3'
    );

SELECT
    *
FROM
    buch
WHERE
    Name LIKE 'Star Wars 1'
    OR Name LIKE 'Star Wars 2'
    OR Name LIKE 'Star Wars 3';

# WHERE Statements verbinden
SELECT
    *
FROM
    buch
WHERE
    ISBN IN (
        '654404044',
        '808442899',
        'A123456789'
    )
    AND Name LIKE 'Star Wars %';

SELECT
    *
FROM
    buch
WHERE
    Name LIKE 'fairy tales%';

SELECT
    *
FROM
    buch
WHERE
    name IN (
        'Fairy tales',
        'Molloy, Malone Dies, The Unnamable, the trilogy',
        'One Thousand and One Nights',
        'Pride and Prejudice',
        'Star Wars 1',
        'Star Wars 12',
        'Star Wars 2',
        'Star Wars 3',
        'Star Wars 4',
        'Star Wars 5',
        'The Book Of Job',
        'The Divine Comedy',
        'The Epic Of Gilgamesh',
        'Things Fall Apart'
    );

# Schüler abfragen, deren ID zwischen 20 und 30 ist
SELECT
    *
FROM
    schueler
WHERE
    SchuelerId LIKE '2_';

SELECT
    *
FROM
    schueler
WHERE
    SchuelerId BETWEEN 20
    AND 30;

SELECT
    *
FROM
    schueler
WHERE
    SchuelerId IN (4, 7, 8, 20, 22);

SELECT
    *
FROM
    schueler
WHERE
    SchuelerId = 4
    OR SchuelerId = 7
    OR SchuelerId = 8
    OR SchuelerId = 20;

# Paul und Besart zu Bücherwurm machen
INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('A123456789', 4),
    ('F123456789', 4),
    ('B123456789', 4),
    ('c123456789', 4),
    ('D123456789', 4),
    ('E123456789', 4),
    ('433408815', 5),
    ('139340034', 5),
    ('159489705', 5),
    ('320764577', 5),
    ('654404044', 5);

# Ausgeliehene Bücher pro Schüler
SELECT
    s.Name,
    COUNT(1) AnzahlBuecherAusgeliehen
FROM
    buch_schueler bs
    LEFT JOIN buch b ON b.ISBN = bs.ISBN
    LEFT JOIN schueler s ON bs.SchuelerId = s.SchuelerId
GROUP BY
    s.Name;

# Anzeige von dem Schüler, der die meisten Bücher ausgeliehen hat
SELECT
    s.Name,
    COUNT(1) AnzahlBuecherAusgeliehen
FROM
    buch_schueler bs
    LEFT JOIN buch b ON b.ISBN = bs.ISBN
    LEFT JOIN schueler s ON bs.SchuelerId = s.SchuelerId
GROUP BY
    s.Name
ORDER BY
    COUNT(1) DESC
LIMIT
    1;
