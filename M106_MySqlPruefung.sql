# Cleanup all existing Tables
DROP TABLE buch_schueler;

DROP TABLE buch;

DROP TABLE stundenplan;

DROP TABLE dozent;

DROP TABLE schueler;

DROP TABLE klasse;

DROP TABLE modul;

DROP TABLE raum;

DROP TABLE wochentag;

-- database
CREATE DATABASE IF NOT EXISTS M106;

USE M106;

# Create all Tables if not existent
CREATE TABLE IF NOT EXISTS buch (
    ISBN char(17) NOT NULL PRIMARY KEY,
    Name char(100) NULL,
    Autor char(100) NULL,
    Gebuehr decimal(10, 2) DEFAULT 0.00 NOT NULL
);

CREATE TABLE IF NOT EXISTS dozent (
    DozentenId int AUTO_INCREMENT PRIMARY KEY,
    Name varchar(50) CHARSET utf8 NOT NULL,
    PLZ int(5) NOT NULL
);

CREATE TABLE IF NOT EXISTS klasse (
    KlassenId int AUTO_INCREMENT PRIMARY KEY,
    Bezeichnung varchar(50) CHARSET utf8 NOT NULL
);

CREATE TABLE IF NOT EXISTS modul (
    ModulId int AUTO_INCREMENT PRIMARY KEY,
    Bezeichnung varchar(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS raum (
    RaumId int AUTO_INCREMENT PRIMARY KEY,
    Bezeichnung varchar(10) NOT NULL,
    Stockwerk int NOT NULL,
    EDV BOOLEAN DEFAULT false NOT NULL
);

CREATE TABLE IF NOT EXISTS schueler (
    SchuelerId int AUTO_INCREMENT PRIMARY KEY,
    Name varchar(50) CHARSET utf8 NOT NULL,
    Klasse int NULL,
    CONSTRAINT FK_SCHUELER_KLASSE FOREIGN KEY (Klasse) REFERENCES klasse (KlassenId)
);

CREATE TABLE IF NOT EXISTS buch_schueler (
    ISBN char(17) NOT NULL,
    SchuelerId int NOT NULL,
    PRIMARY KEY (ISBN, SchuelerId),
    CONSTRAINT FK_BUCH_SCHUELER_BUCH FOREIGN KEY (ISBN) REFERENCES buch (ISBN),
    CONSTRAINT FK_BUCH_SCHUELER_SCHUELER FOREIGN KEY (SchuelerId) REFERENCES schueler (SchuelerId)
);

CREATE TABLE IF NOT EXISTS wochentag (
    WochentagId int NOT NULL PRIMARY KEY,
    Bezeichnung varchar(50) NULL
);

CREATE TABLE IF NOT EXISTS stundenplan (
    Dozent int NOT NULL,
    Klasse int NOT NULL,
    Modul int NOT NULL,
    Raum int NOT NULL,
    Wochentag int NOT NULL,
    UhrzeitStart time NULL,
    UhrzeitEnde time NULL,
    PRIMARY KEY (Dozent, Klasse, Modul, Raum, Wochentag),
    CONSTRAINT FK_STUNDENPLAN_DOZENT FOREIGN KEY (Dozent) REFERENCES dozent (DozentenId),
    CONSTRAINT FK_STUNDENPLAN_KLASSE FOREIGN KEY (Klasse) REFERENCES klasse (KlassenId),
    CONSTRAINT FK_STUNDENPLAN_MODUL FOREIGN KEY (Modul) REFERENCES modul (ModulId),
    CONSTRAINT FK_STUNDENPLAN_RAUM FOREIGN KEY (Raum) REFERENCES raum (RaumId),
    CONSTRAINT FK_STUNDENPLAN_WOCHENTAG FOREIGN KEY (Wochentag) REFERENCES wochentag (WochentagId)
);

# Provide Data for klasse
INSERT INTO
    `klasse` (`KlassenId`, `Bezeichnung`)
VALUES
    (1, 'ITB 1');

INSERT INTO
    `klasse` (`KlassenId`, `Bezeichnung`)
VALUES
    (2, 'ITB 2');

INSERT INTO
    `klasse` (`KlassenId`, `Bezeichnung`)
VALUES
    (3, 'ITB 3');

INSERT INTO
    `klasse` (`KlassenId`, `Bezeichnung`)
VALUES
    (4, 'ITB 4');

# Provide Data for schueler
INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (1, 'Sherie Helversen', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (2, 'Agnes Jouanet', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (3, 'Morgan Keslake', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (4, 'Ivett Velten', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (5, 'Shae Leate', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (6, 'Husein Brosini', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (7, 'Lilyan Papen', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (8, 'Mortie Gheorghe', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (9, 'Nikki Jenicek', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (10, 'Augustin Issacov', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (11, 'Abie Rey', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (12, 'Marlin Greenalf', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (13, 'Shaun Coast', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (14, 'Doris Bateson', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (15, 'Tracee Chomicki', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (16, 'Lauritz Monks', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (17, 'Rhiamon Kevern', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (18, 'Blisse Miklem', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (19, 'Bernadette Saffe', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (20, 'Joey Moneti', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (21, 'Shawn Picardo', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (22, 'Fredrick Kumar', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (23, 'Jennifer Hector', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (24, 'Roarke Rodolico', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (25, 'Sondra Kingsland', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (26, 'Gonzalo Viner', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (27, 'Eran Duplan', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (28, 'Beaufort Tran', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (29, 'Sawyer Espasa', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (30, 'Sabina Bedbury', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (31, 'Alyse Lowdwell', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (32, 'Elmer Eakens', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (33, 'Shalom Fetherstan', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (34, 'Viviene Petyt', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (35, 'Pen Daveley', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (36, 'Moria Geill', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (37, 'Nealy Bendall', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (38, 'Tedman Levane', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (39, 'Griz MacKimmie', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (40, 'Berkie Sanz', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (41, 'Bonny Eckhard', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (42, 'Valida Sharpley', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (43, 'Bessy Scholz', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (44, 'Layne Yeabsley', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (45, 'Gorden Garron', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (46, 'Bibbye Ramm', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (47, 'Darn Petrovic', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (48, 'Gabe Esslement', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (49, 'Eldridge Fretson', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (50, 'Fanni Spikins', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (51, 'Rayner Marson', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (52, 'Dorey Dakers', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (53, 'Marian Trimme', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (54, 'Christian Gregore', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (55, 'Curt Rabbitt', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (56, 'Yasmeen Naish', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (57, 'Kriste Baldacchino', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (58, 'Leontine Sambles', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (59, 'Otto Lace', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (60, 'Kaitlin Windram', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (61, 'Mella Game', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (62, 'Corby Redhead', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (63, 'Cele Franey', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (64, 'Tracie Bontoft', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (65, 'Janek Rannigan', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (66, 'Wayne Secret', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (67, 'Verna Tackett', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (68, 'Shandeigh Mott', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (69, 'Percival Yglesia', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (70, 'Cary Lamball', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (71, 'Lynn Jolin', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (72, 'Lebbie Hamer', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (73, 'Berte Simionato', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (74, 'Brade Fayerbrother', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (75, 'Donal Llorente', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (76, 'Karlee Skeath', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (77, 'Deva Pardal', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (78, 'Davie Glyssanne', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (79, 'Vanessa Jenson', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (80, 'Marlyn Divall', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (81, 'Sibel Dellenty', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (82, 'Costa Gommowe', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (83, 'Tiffy Makin', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (84, 'Nicoline Rugge', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (85, 'Ravid Chatel', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (86, 'Kathryn Knobell', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (87, 'Caesar Demann', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (88, 'Moe Frissell', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (89, 'Devy Aylin', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (90, 'Billi Albasini', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (91, 'Darrelle Curner', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (92, 'Bunny D''Alesco', 1);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (93, 'Nester Tedstone', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (94, 'Gavrielle Hoble', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (95, 'Rosy Maguire', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (96, 'Willow Jedrzejewicz', NULL);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (97, 'Lindy Balazot', 4);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (98, 'Bud Eim', 3);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (99, 'Daria Fontell', 2);

INSERT INTO
    schueler (SchuelerId, Name, Klasse)
VALUES
    (100, 'Sile Pisculli', 4);

# Provide Data for dozent
INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Thedric Dauber', 5641);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Jojo Carnegy', 7171);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Melisse Walbrook', 7563);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Reilly Raine', 5063);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Dulcia Chesney', 7418);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Scotti De Antoni', 6109);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Derby Gozzett', 6616);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Davina Doran', 7735);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Grete Lawrie', 5706);

INSERT INTO
    dozent (Name, PLZ)
VALUES
    ('Gaylene Tregoning', 7178);

# Provide Data for modul
INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (1, 'M155');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (2, 'M394');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (3, 'M224');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (4, 'M130');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (5, 'M103');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (6, 'M182');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (7, 'M377');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (8, 'M344');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (9, 'M333');

INSERT INTO
    modul (ModulId, Bezeichnung)
VALUES
    (10, 'M264');

# Provide Data for raum
INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (1, 0, 'EG 024', false);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (2, 1, 'OG 156', TRUE);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (3, 3, 'OG 323', TRUE);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (4, 2, 'OG 244', TRUE);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (5, 1, 'OG 116', false);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (6, 3, 'OG 324', TRUE);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (7, 1, 'OG 160', TRUE);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (8, 0, 'EG 052', false);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (9, 0, 'EG 075', false);

INSERT INTO
    raum (RaumId, Stockwerk, Bezeichnung, EDV)
VALUES
    (10, 0, 'EG 023', TRUE);

# Provide Data for wochentag
INSERT INTO
    wochentag (WochentagId, Bezeichnung)
VALUES
    (1, 'Montag'),
    (2, 'Dienstag'),
    (3, 'Mittwoch'),
    (4, 'Donnerstag'),
    (5, 'Freitag'),
    (6, 'Samstag'),
    (7, 'Sonntag');

# Provide Data for stundenplan
INSERT INTO
    stundenplan (
        Dozent,
        Klasse,
        Modul,
        Raum,
        Wochentag,
        UhrzeitStart,
        UhrzeitEnde
    )
VALUES
    (1, 1, 3, 2, 6, '13:00:00', '16:35:00');

INSERT INTO
    stundenplan (
        Dozent,
        Klasse,
        Modul,
        Raum,
        Wochentag,
        UhrzeitStart,
        UhrzeitEnde
    )
VALUES
    (1, 1, 4, 2, 6, '08:15:00', '11:55:00');

INSERT INTO
    stundenplan (
        Dozent,
        Klasse,
        Modul,
        Raum,
        Wochentag,
        UhrzeitStart,
        UhrzeitEnde
    )
VALUES
    (2, 2, 2, 4, 2, '18:00:00', '21:45:00');

INSERT INTO
    stundenplan (
        Dozent,
        Klasse,
        Modul,
        Raum,
        Wochentag,
        UhrzeitStart,
        UhrzeitEnde
    )
VALUES
    (3, 3, 1, 3, 2, '18:00:00', '21:45:00');

# Provide Data for buch
INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '399808287-5',
        'Don Quixote',
        7.51,
        'Miguel de Cervantes'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '701285303-4',
        'Alices Adventures in Wonderland',
        1.07,
        'Lewis Carroll'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '245718612-9',
        'The Adventures of Huckleberry Finn',
        11.99,
        'Mark Twain'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '267610802-6',
        'The Adventures of Tom Sawyer',
        14.68,
        'Mark Twain'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '415523434-3',
        'Treasure Island',
        9.04,
        'Robert Louis Stevenson'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '162591778-3',
        'Pride and Prejudice',
        2.97,
        'Jane Austen'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '277351804-7',
        'Wuthering Heights',
        6.82,
        'Emily Brontë'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '101199025-3',
        'Jane Eyre',
        5.86,
        'Charlotte Brontë'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '415958887-5',
        'Moby Dick',
        12.92,
        'Herman Melville'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '236200255-1',
        'The Scarlet Letter',
        7.59,
        'Nathaniel Hawthorne'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '387729593-2',
        'Gullivers Travels',
        8.66,
        'Jonathan Swift'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '626391760-1',
        'The Pilgrims Progress',
        8.58,
        'John Bunyan'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '261802261-0',
        'A Christmas Carol',
        12.33,
        'Charles Dickens'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '559250961-8',
        'David Copperfield',
        4.59,
        'Charles Dickens'
    );

INSERT INTO
    buch (ISBN, Name, Gebuehr, Autor)
VALUES
    (
        '881792256-0',
        'A Tale of Two Cities',
        1.96,
        'Charles Dickens'
    );

# Provide Data for buch_schueler
INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('236200255-1', 42);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 40);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415958887-5', 5);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('261802261-0', 92);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 89);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('101199025-3', 75);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415523434-3', 81);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('881792256-0', 40);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('236200255-1', 69);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415523434-3', 53);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 68);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('236200255-1', 47);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('277351804-7', 76);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('101199025-3', 35);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 59);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('267610802-6', 52);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 87);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('881792256-0', 80);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('101199025-3', 46);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 79);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('399808287-5', 17);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415523434-3', 84);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('245718612-9', 46);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('267610802-6', 14);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 55);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('101199025-3', 1);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('387729593-2', 93);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 19);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415958887-5', 42);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('387729593-2', 47);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 23);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('387729593-2', 6);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 94);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 88);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 84);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('399808287-5', 70);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 68);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 53);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('236200255-1', 93);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('387729593-2', 35);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 9);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('236200255-1', 14);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('236200255-1', 4);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('236200255-1', 78);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('101199025-3', 69);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 21);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 83);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 11);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 42);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('277351804-7', 33);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 98);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415523434-3', 71);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415958887-5', 81);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 32);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 18);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415523434-3', 57);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('881792256-0', 76);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 75);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('245718612-9', 97);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 73);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('387729593-2', 77);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('277351804-7', 75);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('881792256-0', 81);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('236200255-1', 9);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 22);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 84);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('387729593-2', 69);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('261802261-0', 86);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('387729593-2', 86);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 34);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 51);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 63);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 18);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('399808287-5', 66);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('399808287-5', 28);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('245718612-9', 87);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('387729593-2', 2);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 91);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 58);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 21);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415958887-5', 68);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 2);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('415523434-3', 68);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('267610802-6', 55);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('277351804-7', 5);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 83);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('277351804-7', 51);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 20);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 38);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('267610802-6', 85);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('626391760-1', 62);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('267610802-6', 7);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('162591778-3', 41);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('277351804-7', 31);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('559250961-8', 94);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('881792256-0', 70);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('101199025-3', 27);

INSERT INTO
    buch_schueler (ISBN, SchuelerId)
VALUES
    ('701285303-4', 53);
